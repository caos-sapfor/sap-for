package alpha.sercaos.test;
/*
import alpha.sercaos.api.CandidatureManager;
import com.sun.jersey.api.core.ResourceConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import fr.istic.caos.ccn.TestSapfor.SimpleTest.HelloResource;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

@SuppressWarnings("unused")
public class TestCandidatureManager  extends JerseyTest{
	
	//@Override
    protected Application configure() {
        return new ResourceConfig(CandidatureManager.class);
    }
 
    @Test
    public void desinscriptionSessionUV() {
        
    	Response response = target("01234567890123456789/candidatures/111222333444555").request().delete();
        String hello = response.readEntity(String.class);
        assertEquals("{\"message\" : String \"candidatureSupprimée\",\"sessionUV\":{\"idSessionUV\":\"666\",\"intitule\":\"Traverser l'enfer\",\"typeUV\"=\"Stage de survie\",\"dates\":[{ \"jour\":15, \"mois\":2,\"annee\":2018},{\"jour\":16, \"mois\":2,\"annee\":2018}]}}", hello);
        response.close();      
    }
    
    @Test
    public void consulterCandidatures() {
        
    	Response response = target("01234567890123456789/candidatures").request().get();
        String hello = response.readEntity(String.class);
        assertEquals(
        "{\r\n" + 
        "{\"idSessionUV\":\"666\",\"intitule\":\"Traverser l'enfer\",\"typeUV\"=\"Stage de survie\",\"dates\":[{ \"jour\":15, \"mois\":2,\"annee\":2018},{\"jour\":16, \"mois\":2,\"annee\":2018}]},\r\n" + 
        "{\"idSessionUV\":\"66\",\"intitule\":\"Traverser la rue\",\"typeUV\"=\"Stage de survie\",\"dates\":[{ \"jour\":14, \"mois\":2,\"annee\":2018},{\"jour\":14, \"mois\":2,\"annee\":2018}]},\r\n" + 
        "{\"idSessionUV\":\"6\",\"intitule\":\"Sortir du lit\",\"typeUV\"=\"Stage de survie\",\"dates\":[{ \"jour\":13, \"mois\":2,\"annee\":2018},{\"jour\":13, \"mois\":2,\"annee\":2018}]}\r\n" + 
        "}", hello);
        response.close();      
    }
    
    @Test
    public void afficherCandidatureSessionUV() {
    	        
    	Response response = target("01234567890123456789/candidatures/idSession?id=666").request().get();
    	String hello = response.readEntity(String.class);
    	assertEquals(
    			"{\r\n" + 
    			"  {\"idAgent\": \"marcel1\", \"idSessionUV\": \"feu_foret_2\", \"statut\": \"en_attente\", \"qualite\": \"stagiaire\", \"position\": 12}, \r\n" + 
    			"  {\"idAgent\": \"marcel2\", \"idSessionUV\": \"feu_foret_2\", \"statut\": \"accepte\", \"qualite\": \"formateur\", \"position\": 1}\r\n" + 
    			"}", hello);
    	response.close();      
    }
    
    @Test
    public void accepterCandidature() {
    	        
    	Response response = target("01234567890123456789/candidatures/qualifierCand?idSUV=666&idCand=111222333444555&statCand=\"accepte\"").request().post(null);
    	String hello = response.readEntity(String.class);
    	assertEquals(
    			"{\"Message\" : \"candidature acceptée\", \r\n" + 
    			"\"candidaure\":{\"idAgent\":\"256\",\"idSessionUV\":\"666\",\"statut\":\"en attente\",\"qualite\":\"apprenant\",\"position\":null}}", hello);
    	response.close();      
    }  
    
    @Test
    public void refuserCandidature() {
    	        
    	Response response = target("01234567890123456789/candidatures/qualifierCand?idSUV=666&idCand=111222333444555&statCand=\"refuse\"").request().post(null);
    	String hello = response.readEntity(String.class);
    	assertEquals(
    			"{\"Message\" : \"candidature refusée\", \r\n" + 
    			"\"candidaure\":{\"idAgent\":\"256\",\"idSessionUV\":\"666\",\"statut\":\"refuse\",\"qualite\":\"apprenant\",\"position\":null}}", hello);
    	response.close();      
    }       
    
    @Test
    public void MettreEnAttente() {
    	        
    	Response response = target("01234567890123456789/candidatures/qualifierCand?idSUV=666&idCand=111222333444555&statCand=\"en_attente\"").request().post(null);
    	String hello = response.readEntity(String.class);
    	assertEquals(
    			"{\"Message\" : \"candidature en attente\", \r\n" + 
    			"\"candidaure\":{\"idAgent\":\"256\",\"idSessionUV\":\"666\",\"statut\":\"en_attente\",\"qualite\":\"apprenant\",\"position\":3}}", hello);
    	response.close();      
    }   
    
    @Test
    public void modifierEnListeDAttente() {
    	        
    	Response response = target("01234567890123456789/candidatures/repositionerCandidSUV=666&idCand=111222333444555&nouvellePosition=1").request().post(null);
    	String hello = response.readEntity(String.class);
    	assertEquals(
    			"{\r\n" + 
    			" {\"idAgent\": \"marcel1\", \"idSessionUV\": \"feu_foret_2\", \"statut\": \"en_attente\", \"qualite\": \"stagiaire\", \"position\": 1}, \r\n" + 
    			" {\"idAgent\": \"marcel2\", \"idSessionUV\": \"feu_foret_2\", \"statut\": \"en_attente\", \"qualite\": \"formateur\", \"position\": 2},\r\n" + 
    			"{\"idAgent\": \"marcel3\", \"idSessionUV\": \"feu_foret_2\", \"statut\": \"en_attente\", \"qualite\": \"stagiaire\", \"position\": 3}, \r\n" + 
    			"{\"idAgent\": \"marcel4\", \"idSessionUV\": \"feu_foret_2\", \"statut\": \"en_attente\", \"qualite\": \"stagiaire\", \"position\": 4}\r\n" + 
    			"}", hello);
    	response.close();      
    }   
}
*/