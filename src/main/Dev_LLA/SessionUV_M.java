package ALFA_Group;

import java.util.List;

public class SessionUV_M {
    private String idSessionUV;
    private String intitule;
    private TypeSession_M typeUV;
    private List<String> date;
    private List<Candidatures_M> list_candidatures;

    // constructor with parameters
    public SessionUV_M(String intitule, TypeSession_M unTypeUV ) {
        super();
        this.idSessionUV = Integer.toHexString(this.hashCode());
        this.intitule = intitule;
        this.typeUV = unTypeUV;
    }

    // constructor without parameters
    public SessionUV_M() {
        super();
    }

    // renvoie la liste de dates
    public List<String> getDate() {
        return date;
    }
    // ajouter une date à la liste de dates
    public void addDate(String uneDate){
        this.date.add(uneDate);
    }

    // renvoie la liste des candidatures de la session
    public List<Candidatures_M> getList_candidatures() {
        return list_candidatures;
    }

    // Ajoute une candidature à la liste des candidatures existantes pour la session
    public void addCandidature(Candidatures_M uneCandidature){
        this.list_candidatures.add(uneCandidature);
    }

    // Setters et getters sur les autres attributs

    public String getIdSessionUV() {
        return idSessionUV;
    }

    public void setIdSessionUV(String idSessionUV) {
        this.idSessionUV = idSessionUV;
    }


    public String getIntitule() {
        return intitule;
    }


    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public TypeSession_M getTypeUV() {
        return typeUV;
    }

    public void setTypeUV(TypeSession_M typeUV) {
        this.typeUV = typeUV;
    }
}

