package ALFA_Group;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Agent_M")
public class Agent {
    String idAgent;
    String nom;
    String login;
    String password;
    Boolean estGestionnaire;


    // Succession de 3 Getters pour chacun des attributs d'un Agent_M
    public String getIdAgent(){
        return this.idAgent;
    }

    public void setIdAgent(String un_idAgent){
        this.idAgent = un_idAgent;
    }


    public String getNom(){
        return this.nom;
    }

    public void setNom(String un_nom){
        this.nom = un_nom;
    }

    public String getLogin(){
        return this.login;
    }

    public void setLogin(String unlogin){
        this.login = unlogin;
    }

    //Constructeur sans paramètre
    public Agent(){
    }

    // Constructeur du DTO Agent_M avec 3 paramètres
    public Agent(String idAgent, String nom, String login, String password, boolean gestionnaire){
        this.idAgent = idAgent;
        this.nom = nom;
        this.login = login;
        this.password = password;
        this.estGestionnaire = gestionnaire;

    }
}