package fr.istic.caos.ccn.TestSapfor;


import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

public class SimpleTest extends JerseyTest {
	 
    @Path("hello")
    public static class HelloResource {
        @GET
        public String getHello() {
            return "Hello World!";
        }
        
        @POST
        public String postHello() {
            return "Hello World!";
        }
        
        @PUT
        public String putHello() {
            return "Hello World!";
        }
        
    }
 
    //@Override
    protected Application configure() {
        return new ResourceConfig(HelloResource.class);
    }
 
    @Test
    public void test() {
        
    	Response response = target("hello").request().get();
        String hello = response.readEntity(String.class);
        assertEquals("Hello World!", hello);
        response.close();      
    }
    
    @Test
    public void test2() {
        
    	Response response = target("hello").request().post(null);
        String hello = response.readEntity(String.class);
        assertEquals("Hello World!", hello);
        response.close();   
    }

    @Test
    public void test3() {
        
    	Response response = target("hello").request().put(null);
        String hello = response.readEntity(String.class);
        assertEquals("Hello World!", hello);
        response.close();   
    }
}