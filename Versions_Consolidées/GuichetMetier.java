package ALFA_Group;
import com.sun.jersey.spi.resource.Singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@Singleton

// La classe GuichetMetier implemente l'interface Metier_I
public class GuichetMetier {
    // pour être utilisé cette classe utilise nécéssairement une instance de sa propre classe
    private static GuichetMetier guichetmetier;

    // La classe GuichetMetier comporte une liste des objets de chaque classes métiers afin de pouvoir contenir l'ensemble
    // des instances des objets métiers existants

    private List<Agent_M> globale_list_agents;
    private List<Candidature_M> globale_list_candidatures;
    private List<SessionUV_M> globale_list_sessionUV;
    private List<TypeSession_M> globale_list_type_session;
    private HashMap<String, Agent_M> list_couple_SID_Agent_M;

    // Constructeur dédié aux tests, initialise le Guichetmetier avec des valeurs dans les différentes listes
    private GuichetMetier(){
        this.globale_list_agents = new ArrayList<Agent_M>();
        this.globale_list_type_session = new ArrayList<TypeSession_M>();
        this.globale_list_sessionUV = new ArrayList<SessionUV_M>();

        TypeSession_M TypeSessEx01 = new TypeSession_M("Initiation lance à eau", 15, 5);
        TypeSession_M TypeSessEx02 = new TypeSession_M("Monter à la grande échelle", 20, 3);
        globale_list_type_session.add(TypeSessEx01);
        globale_list_type_session.add(TypeSessEx02);
        SessionUV_M sessionUV_M01 = new SessionUV_M(TypeSessEx01, "22-05-18");
        SessionUV_M sessionUV_M02 = new SessionUV_M(TypeSessEx01, "12-06-18");
        SessionUV_M sessionUV_M03 = new SessionUV_M(TypeSessEx02, "09-04-18");
        SessionUV_M sessionUV_M04 = new SessionUV_M(TypeSessEx02, "22-05-18");
        SessionUV_M sessionUV_M05 = new SessionUV_M(TypeSessEx02, "15-06-18");
        globale_list_sessionUV.add(sessionUV_M01);
        globale_list_sessionUV.add(sessionUV_M02);
        globale_list_sessionUV.add(sessionUV_M03);
        globale_list_sessionUV.add(sessionUV_M04);
        globale_list_sessionUV.add(sessionUV_M05);
        Agent_M agent_M01 = new Agent_M( "BERGER", "BE0001", "mypassword001", false);
        Agent_M agent_M02 = new Agent_M( "MARTIN", "MA0001", "mypassword002", false);
        Agent_M agent_M03 = new Agent_M( "DUPONT", "DU0001", "mypassword003", false);
        Agent_M agent_M04 = new Agent_M( "GALL", "GA0001", "mypassword004", true);
        Agent_M agent_M05 = new Agent_M( "DUPONT", "DU0001", "mypassword005", false);
        Agent_M agent_M06 = new Agent_M( "NOHA", "NO0001", "mypassword006", false);
        Agent_M agent_M07 = new Agent_M( "ZIDANE", "ZI0001", "mypassword007", false);
        Agent_M agent_M08 = new Agent_M( "EVRA", "EV0001", "mypassword008", false);
        Agent_M agent_M09 = new Agent_M( "LEWIS", "LE0001", "mypassword009", false);
        globale_list_agents.add(agent_M01);
        globale_list_agents.add(agent_M02);
        globale_list_agents.add(agent_M03);
        globale_list_agents.add(agent_M04);
        globale_list_agents.add(agent_M05);
        globale_list_agents.add(agent_M06);
        globale_list_agents.add(agent_M07);
        globale_list_agents.add(agent_M08);
        globale_list_agents.add(agent_M09);
        Candidature_M candidature_M01 = new Candidature_M(agent_M01, sessionUV_M01, "Accepted", "Stagiaire", 0);
        Candidature_M candidature_M02 = new Candidature_M(agent_M01, sessionUV_M02, "Rejected", "Stagiaire", 0);
        Candidature_M candidature_M03 = new Candidature_M(agent_M02, sessionUV_M01, "Processing", "Stagiaire", 0);
        Candidature_M candidature_M04 = new Candidature_M(agent_M03, sessionUV_M03, "Accepted", "Formateur", 0);
        globale_list_candidatures.add(candidature_M01);
        globale_list_candidatures.add(candidature_M02);
        globale_list_candidatures.add(candidature_M03);
        globale_list_candidatures.add(candidature_M04);

    }

    //Ce Getter permet aux différents managers d'appeler l'instance existante de la classe métier.
    //Si il n'existe pas d'instance de la classe Metier au moment de l'appelle, alors une instance est créée
    public static GuichetMetier getGuichetMetier(){
        if (guichetmetier == null){
            guichetmetier = new GuichetMetier();
        }
        return guichetmetier;
    }

        /*
    // Afficher la liste des sessions accessibles pour un utilisateur
    public List<SessionUV_M> getSessionsAccessibles_V2(String SID){
        List<TypeSession_M> validTypeSession;
        Agent_M targeted_Agent = check_SID_Agent_M(SID);
        for (int i =0; i < globale_list_type_session.size(); i++ ){
            TypeSession_M typeSessAnalized = globale_list_type_session.get(i);
            for (int j =0; j < typeSessAnalized.list_typePrereq.size(); j++){

            }

        }
    }
*/

    /////// Toutes les méthodes suivantes sont des méthodes de test, à appeler depuis vos managers le temps que je rédige le vrai code.
    ///////
    public List<SessionUV_M> getSessionsAccessibles (String SID){
        return globale_list_sessionUV;
    }

    public SessionUV_M getDetailsSession(String idSessionUV){
        return new SessionUV_M(new TypeSession_M("Stage de survie", 20, 3), "22-09-22");
    }

    public Candidature_M createCandidature(String SID, String idSessionUV){
        return new Candidature_M();
    }

    public boolean suppressCandidature(Candidature uneCandidature){
        return true;
    }

    public List<Candidature_M> getListCandidatures_M(String SID){
        return globale_list_candidatures;
    }

    //


    public HashMap<String, Agent_M> getList_couple_SID_Agent_M() {
        return list_couple_SID_Agent_M;
    }

    public boolean estConnexion(String SID){
        return true;
    }

    //Cette fonction renvoie la liste contenant l'ensemble des objets Agent_M existant
    public List<Agent_M> getGlobale_list_agents(){
        return globale_list_agents;


    }
    private Agent_M check_SID_Agent_M(String SID){
        return this.list_couple_SID_Agent_M.get(SID);
    }



    private class List_SID_Agent_M {

        List_SID_Agent_M(String SID, Agent_M unAgent_M){
            List couple_SID_Agent_M = new ArrayList();
            couple_SID_Agent_M.add(SID);
            couple_SID_Agent_M.add(unAgent_M);
        }
    }


}
