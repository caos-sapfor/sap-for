package ALFA_Group;

import com.sun.jersey.spi.resource.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Viviane Hounkanrin
 */
@Singleton
@Path("sid/sessions")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class SessionsManager {


    private List<SessionUV_M> sessionList;


    // Default action for SessionUV_M Manager: return the test sessions and return them
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public synchronized List<SessionUV_M> getSessionList(@QueryParam("sid") String sid, @Context HttpServletRequest request) {
        List<SessionUV> sessionUVS = new ArrayList<SessionUV>();
        HttpSession sessionInformatique = request.getSession();
        //verification de la connexion
        if (sessionInformatique.getId().equals(sid)) {
            Agent_M agentMConnecte = (Agent_M) sessionInformatique.getAttribute("agentMConnecte");
            if (sessionInformatique.getId().equals(sid)) {
                for (SessionUV_M session : this.sessionList) {
                    SessionUV sessionUV = new SessionUV(session);
                    sessionUVS.add(sessionUV);
                }
                return this.sessionList;
            }
        }
        return null;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{idSessionUV}")
    public synchronized SessionUV_M getSessionsDetails(@QueryParam("sid") String sid, @PathParam("idSessionUV") String idSessionUV, @Context HttpServletRequest request) {
        //List<SessionUV> sessionDTOs = new ArrayList<SessionUV>() ;
        //recuperation de la session
        HttpSession sessionInformatique = request.getSession();
        // verification si l'agent vraiment connecté
        if (sessionInformatique.getId().equals(sid)) {
            Agent_M agentMConnecte = (Agent_M) sessionInformatique.getAttribute("agentMConnecte");
            //parcours de la liste des sessions
            for (SessionUV_M session : this.sessionList) {
                if (session.getIdSessionUV().equals(idSessionUV))
                    return session;
            }
        }
        return null;
    }

	/*@POST
	@Path("/cand")
	@Produces({MediaType.APPLICATION_JSON})
	//@Consumes({MediaType.APPLICATION_JSON})

	public synchronized String CandidaterSessionsUV (@QueryParam("sid") String sid, @QueryParam("idSessionUV") String idSessionUV, @QueryParam("qualite") String qualite, @QueryParam("date") Date date, @QueryParam("prerequis")  String prerequis, @Context HttpServletRequest request){
		List<SessionUV> sessionUVS = new ArrayList<SessionUV>();
		Candidature_M candidatureM;
		SessionUV_M session;
			//recuperation de la session
		HttpSession sessionInformatique = request.getSession();
		// verification si l'agent vraiment connecté
		if (sessionInformatique.getId().equals(sid)) {
			Agent_M agentMConnecte = (Agent_M) sessionInformatique.getAttribute("agentMConnecte");
			//vérification de la qualité



			//verification de la validité de la date
			for ( session : this.sessionList)


				if (session.getDate().equals(date) && session.getIdSessionUV().equals(idSessionUV)


		return "";
	}
}
}

	  // Add a new product to the list.
	/*@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public synchronized SessionUV_M addSession(SessionUV_M s) {
		this.sessionList.add(s);
		return s;
	}

	// get session by id
	//  @param id is the id of the session
	// @return the session with the corresponding id, null if there's none.
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("find")
	public synchronized SessionUV_M getSessionByName(@QueryParam("id") String id) {
		for (SessionUV_M s : this.productList) {
			if (s.getID().equals(id))
				return s;
		}
		return null;
	}

	// Get a session with its title(intitulé)
	// @param intitule is the title of the session
	// @return the session with the corresponding title, null if there's none.
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("find")
	public synchronized SessionUV_M getSessionByIntitule(@QueryParam("intitule") String intitule) {
		for (SessionUV_M s : this.sessionList) {
			if (s.getIntitule().equals(intitule))
				return s;
		}
		return null;
	}*/

}
