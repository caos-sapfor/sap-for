package ALFA_Group;
import com.sun.jersey.spi.resource.Singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Singleton

// La classe GuichetMetier implemente l'interface Metier_I
public class GuichetMetier {
    // pour être utilisé cette classe utilise nécéssairement une instance de sa propre classe
    private static GuichetMetier guichetmetier;

    // La classe GuichetMetier comporte une liste des objets de chaque classes métiers afin de pouvoir contenir l'ensemble
    // des instances des objets métiers existants

    private List<Agent_M> globale_list_agents;
    private List<Candidatures_M> globale_list_candidatures;
    private List<SessionUV_M> globale_list_sessionUV;
    private List<TypeSession_M> globale_list_type_session;
    private HashMap<String, Agent_M> list_couple_SID_Agent_M;


    //Ce Getter permet aux différents managers d'appeler l'instance existante de la classe métier.
    //Si il n'existe pas d'instance de la classe Metier au moment de l'appelle, alors une instance est créée
    public static GuichetMetier getGuichetMetier(){
        if (guichetmetier == null){
            guichetmetier = new GuichetMetier();
        }
        return guichetmetier;
    }

    //Cette fonction renvoie la liste contenant l'ensemble des objets Agent_M existant
    public List<Agent_M> getGlobale_list_agents(){
        return globale_list_agents;


    }

    //Cette fonction créé un Agent_M à partir d'un Agent (DTO), l'ajoute à la liste qui contient l'ensemble des agents
    // puis renvoie l'instance de l'Agent_M créée.
    public Agent_M create_Agent(Agent unAgent){
        String nom_M = unAgent.nom;
        String login_M = unAgent.login;
        String password_M = unAgent.password;
        boolean gestionnaire_M = unAgent.estGestionnaire;
        globale_list_agents.add(new Agent_M(nom_M,login_M, password_M, gestionnaire_M));
        return globale_list_agents.get(globale_list_agents.size());
    }

    //Cette fonction est juste pour mes tests, elle créé une liste d'Agent (DTO)
    public void creaList_Agents() {

        List<Agent> Liste_agents = new ArrayList<Agent>();

        Liste_agents.add(new Agent("22RE44", "BERGER", "BE0001", "mypassword001", false));
        Liste_agents.add(new Agent("89ZY54", "MARTIN", "MA0001", "mypassword002", false));
        Liste_agents.add(new Agent("45DD12", "DUPONT", "DU0001", "mypassword003", false));
        Liste_agents.add(new Agent("67HH90", "GALL", "GA0001", "mypassword004", true));


    }

}
