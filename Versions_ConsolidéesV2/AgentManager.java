package ALFA_Group;
import com.sun.jersey.spi.resource.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Singleton
@Path("agents")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})

public class AgentManager {

    private List<Agent_M> Liste_agents_M;
    private List<Agent> Liste_agents;

    // Action sur GET
    // Déclenche la méthode de la classe "Metier" "getGlobale_list_agents" qui donne la liste de toutes les objets Agent_M
    // A partir de cette liste d'agents_M, on reconstruit une liste d'Agents(DTO), pour renvoyer au demandeur
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public synchronized List<Agent> getAgent_List() {
        GuichetMetier managerMetier = GuichetMetier.getGuichetMetier();
        Liste_agents_M = managerMetier.getGlobale_list_agents();
            for (int i = 0; i < Liste_agents_M.size(); i++){
                String idAgent = Liste_agents_M.get(i).getIdAgent();
                String nom = Liste_agents_M.get(i).getNom();
                String login = Liste_agents_M.get(i).getLogin();
                String mdp = Liste_agents_M.get(i).getPassword();
                boolean estgestionnaire = Liste_agents_M.get(i).isGestionnaire();

                Liste_agents.add(new Agent(idAgent, nom, login, mdp, estgestionnaire));
            }

        return Liste_agents;
    }


    // Action sur POST
    // créé un Agent_M en appelant la méthode de la classe "Métier" "create_Agent", cette méthode renvoie l'agent_M créé
    // les caractéristique de l'agent_M sont remises sous format DTO pour retransmission à celui qui a demandé la création de l'agent
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public synchronized Agent addAgent(Agent newAgent){
        Agent_M nouvAgent = Metier.create_Agent(newAgent);
        return new Agent(nouvAgent.getIdAgent(), nouvAgent.getNom(), nouvAgent.getLogin(), nouvAgent.getPassword(), nouvAgent.isGestionnaire());
    }



}

