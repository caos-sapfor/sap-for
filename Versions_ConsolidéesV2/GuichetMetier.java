package ALFA_Group;
import com.sun.jersey.spi.resource.Singleton;

import java.util.*;

@Singleton

// La classe GuichetMetier implemente l'interface Metier_I
public class GuichetMetier {
    // pour être utilisé cette classe utilise nécéssairement une instance de sa propre classe
    private static GuichetMetier guichetmetier;

    // La classe GuichetMetier comporte une liste des objets de chaque classes métiers afin de pouvoir contenir l'ensemble
    // des instances des objets métiers existants

    private List<Agent_M> globale_list_agents;
    private List<Candidature_M> globale_list_candidatures;
    private List<SessionUV_M> globale_list_sessionUV;
    private List<TypeSession_M> globale_list_type_session;
    private Map<String, Agent_M> list_couple_SID_Agent_M;




    // Constructeur dédié aux tests, initialise le Guichetmetier avec des valeurs dans les différentes listes
    private GuichetMetier(){
        this.globale_list_agents = new ArrayList<Agent_M>();
        this.globale_list_type_session = new ArrayList<TypeSession_M>();
        this.globale_list_sessionUV = new ArrayList<SessionUV_M>();
        this.list_couple_SID_Agent_M = (new HashMap<String, Agent_M>());

        TypeSession_M TypeSessEx01 = new TypeSession_M("Initiation lance à eau", 15, 5);
        TypeSession_M TypeSessEx02 = new TypeSession_M("Monter à la grande échelle", 20, 3);
        globale_list_type_session.add(TypeSessEx01);
        globale_list_type_session.add(TypeSessEx02);
        SessionUV_M sessionUV_M01 = new SessionUV_M(TypeSessEx01, "22-05-18");
        SessionUV_M sessionUV_M02 = new SessionUV_M(TypeSessEx01, "12-06-18");
        SessionUV_M sessionUV_M03 = new SessionUV_M(TypeSessEx02, "09-04-18");
        SessionUV_M sessionUV_M04 = new SessionUV_M(TypeSessEx02, "22-05-18");
        SessionUV_M sessionUV_M05 = new SessionUV_M(TypeSessEx02, "15-06-18");
        globale_list_sessionUV.add(sessionUV_M01);
        globale_list_sessionUV.add(sessionUV_M02);
        globale_list_sessionUV.add(sessionUV_M03);
        globale_list_sessionUV.add(sessionUV_M04);
        globale_list_sessionUV.add(sessionUV_M05);
        Agent_M agent_M01 = new Agent_M( "BERGER", "BE0001", "mypassword001", false);
        Agent_M agent_M02 = new Agent_M( "MARTIN", "MA0001", "mypassword002", false);
        Agent_M agent_M03 = new Agent_M( "DUPONT", "DU0001", "mypassword003", false);
        Agent_M agent_M04 = new Agent_M( "GALL", "GA0001", "mypassword004", true);
        Agent_M agent_M05 = new Agent_M( "DUPONT", "DU0001", "mypassword005", false);
        Agent_M agent_M06 = new Agent_M( "NOHA", "NO0001", "mypassword006", false);
        Agent_M agent_M07 = new Agent_M( "ZIDANE", "ZI0001", "mypassword007", false);
        Agent_M agent_M08 = new Agent_M( "EVRA", "EV0001", "mypassword008", false);
        Agent_M agent_M09 = new Agent_M( "LEWIS", "LE0001", "mypassword009", false);
        globale_list_agents.add(agent_M01);
        globale_list_agents.add(agent_M02);
        globale_list_agents.add(agent_M03);
        globale_list_agents.add(agent_M04);
        globale_list_agents.add(agent_M05);
        globale_list_agents.add(agent_M06);
        globale_list_agents.add(agent_M07);
        globale_list_agents.add(agent_M08);
        globale_list_agents.add(agent_M09);
        Candidature_M candidature_M01 = new Candidature_M(agent_M01, sessionUV_M01, "Accepted", "Stagiaire", 0);
        Candidature_M candidature_M02 = new Candidature_M(agent_M01, sessionUV_M02, "Rejected", "Stagiaire", 0);
        Candidature_M candidature_M03 = new Candidature_M(agent_M02, sessionUV_M01, "Processing", "Stagiaire", 0);
        Candidature_M candidature_M04 = new Candidature_M(agent_M03, sessionUV_M03, "Accepted", "Formateur", 0);
        globale_list_candidatures.add(candidature_M01);
        globale_list_candidatures.add(candidature_M02);
        globale_list_candidatures.add(candidature_M03);
        globale_list_candidatures.add(candidature_M04);

    }

    //Ce Getter permet aux différents managers d'appeler l'instance existante de la classe métier.
    //Si il n'existe pas d'instance de la classe Metier au moment de l'appelle, alors une instance est créée
    public static GuichetMetier getGuichetMetier(){
        if (guichetmetier == null){
            guichetmetier = new GuichetMetier();
        }
        return guichetmetier;
    }





    ////////////////////////// Ensemble des méthodes qui concernant les SessionsUV  ////////////////////////////////

    // Cette methode renvoie la liste des SessionsUV accessibles pour un agent donné (à partir du SID en paramètre)
    public List<SessionUV_M> getSessionsAccessibles (String SID){

        Agent_M agent;

        agent=this.getAgentFromSid(SID); //récupération de l'agent correspondant au sid



        List<SessionUV_M> resultat = new LinkedList<SessionUV_M>();//initialisation de la liste résultat



        for(TypeSession_M a:this.globale_list_type_session) {//pour chaque typeUV

            if(agent.getList_UV_achieved().containsAll(a.getList_typePrereq())) {// si les prérequis sont tous dans les aquis de l'agent

                resultat.addAll(a.getList_sessions());//ajout des sessionsUV du typeUV dans la liste

            }

        }

        return resultat;

    }

    // Methode qui renvoie une SessionUV_M (Métier) à partir de l'idSession
    public SessionUV_M getDetailsSession(String idSessionUV){

        boolean trouver =false;// session non encore trouvée

        ListIterator<SessionUV_M> listeLocale=globale_list_sessionUV.listIterator();//création d'un itérateur sur la liste des sessions

        SessionUV_M session=null;//initialisation de la session à retourner



        while(listeLocale.hasNext()&&!trouver) {//parcourt de la liste jusqu'à terminer la liste ou trouver la bonne session

            session = listeLocale.next();//récupération de la session suivante

            if(idSessionUV==session.getIdSessionUV()) {trouver=true;}//s'il s'agit de la bonne session alors on a trouvé et on sort de la boucle

        }



        if(!trouver) {session=null;} //si sortie de la boucle car fin de la liste alors on élimine la dernière valeure de session



        return session;//new SessionUV_M(new TypeSession_M("Stage de survie", 20, 3), "22-09-22");

    }

    // Methode pour récupérer la liste des candidatures à une SessionUV donnée
    public List<Candidature_M> getListCandidaturesPourUneSessionUV(String idSessionUV){
        boolean trouver =false;// SesionUV non encore trouvée

        ListIterator<SessionUV_M> listeLocale=this.globale_list_sessionUV.listIterator();//création d'un itérateur sur la liste des sessionsUV

        SessionUV_M session=null;//initialisation de la sessionUV pour laquelle on recherche la liste de candidatures à retourner

        while(listeLocale.hasNext()&&!trouver) {//parcourt de la liste jusqu'à terminer la liste ou trouver le bon agent

            session = listeLocale.next();//récupération de la sessionUV_M suivante suivant

            if(idSessionUV == session.getIdSessionUV()) {trouver=true;}//s'il s'agit de la bonne session alors on a trouvé et on sort de la boucle

        }

        if(!trouver) {session=null;} //si sortie de la boucle car fin de la liste alors on élimine la dernière valeure de session

        return session.getList_candidatures();

    }






    ////////////////////////// Ensemble des méthodes qui concernant les Candidatures  ////////////////////////////////

    // Methode pour créer une nouvelle candidature pour un Agent, à partir de SID, de l'idSession et qualité
    public Candidature_M createCandidature(String SID, String idSessionUV,String qualite){

        Agent_M agent=this.getAgentFromSid(SID);

        SessionUV_M session=this.getDetailsSession(idSessionUV);//récupération des paramètres



        Candidature_M cand= new Candidature_M( agent, session, "pending", qualite, 0);//création de la candidature

        //ajout de la candidature dans la liste de l'objet agent et session correspondant

        agent.addCandidatures(cand);

        session.addCandidature(cand);

        this.globale_list_candidatures.add(cand);

        return cand;

    }

    //Methode qui supprime une candidature à partir d'une Candidature (DTO) fournie en paramètre
    public boolean suppressCandidature(Candidature uneCandidature){

        Candidature_M cand=this.getCandidature_M(uneCandidature);

        cand.delete(this);//suprimer de la liste globale//suprimer de la liste de l'agent//suprimer de la liste de la session

        return true;

    }

    //Methode pour mettre à jour une candidature (Accepter, refuser ou mettre en attente)
    public boolean updateCandidature(Candidature uneCandidature, String statut){
        Candidature_M Cand = getCandidature_M(uneCandidature);
        if (statut == "accepte"){Cand.setStatutCandidature("accepte");}
        else if (statut == "refuse"){Cand.setStatutCandidature("refuse");}
        else if (statut == "en_attente"){Cand.setStatutCandidature("en_attente");}
        else if (statut == "soumise"){Cand.setStatutCandidature("soumise");}

        return true;

    }

    //Methode pour changer la position d'une candidature dans la liste d'attente
    public int updateCandidaturePosition(Candidature uneCandidature, int position){
        Candidature_M Cand = getCandidature_M(uneCandidature);
        Cand.setPosition(position);
        return Cand.getPosition();
    }


    //Methode utilisée pour renvoyer la liste des candidatures d'un Agent à partir de son SID
    public List<Candidature_M> getListCandidatures_M(String SID){

        Agent_M agent=this.getAgentFromSid(SID);

        return agent.getCandidatures();

    }

    // Methode pour simplement renvoyer la liste globale des candidatures du système
    public List<Candidature_M> getListCandidatures_M(){
        return globale_list_candidatures;
    }

    // cette méthode permet de récupérer une Candidature_M à partir d'une Candidature(DTO)
    private Candidature_M getCandidature_M(Candidature cand_DTO) {

        Candidature_M cand=null;//initialisation de la candidature à retourner

        Agent_M agent =this.getAgentFromId(cand_DTO.getIdAgent());

        SessionUV_M session=this.getDetailsSession(cand_DTO.getIdSession());



        boolean trouver =false;// session non encore trouvée

        ListIterator<Candidature_M> listeLocale=this.globale_list_candidatures.listIterator();//création d'un itérateur sur la liste des candidatures



        while(listeLocale.hasNext()&&!trouver) {//parcourt de la liste jusqu'à terminer la liste ou trouver la bonne candidature

            cand = listeLocale.next();//récupération de la candidature suivante

            if(agent==cand.getAgent()&&session==cand.getSessionUV()) {trouver=true;}//s'il s'agit de la bonne session alors on a trouvé et on sort de la boucle

        }



        if(!trouver) {cand=null;}



        return cand;

    }




    ////////////////////////// Ensemble des méthodes qui concernant les Agents ////////////////////////////////


    //Cette fonction renvoie la liste contenant l'ensemble des objets Agent_M existant
    public List<Agent_M> getGlobale_list_agents(){
        return globale_list_agents;
    }

    // Cette méthode renvoie un Agent_M à partir d'un SID
    private Agent_M getAgentFromSid(String SID){

        Agent_M agent;

        agent=this.list_couple_SID_Agent_M.getOrDefault(SID, null);

        return agent;

    }

    // Cette méthode permet de récupérer un Agent_M à partir de son idAgent
    private Agent_M getAgentFromId(String idAgent) {

        boolean trouver =false;// agent non encore trouvée

        ListIterator<Agent_M> listeLocale=this.globale_list_agents.listIterator();//création d'un itérateur sur la liste des agents

        Agent_M agent=null;//initialisation de l'agent à retourner



        while(listeLocale.hasNext()&&!trouver) {//parcourt de la liste jusqu'à terminer la liste ou trouver le bon agent

            agent = listeLocale.next();//récupération de l'agent suivant

            if(idAgent==agent.getIdAgent()) {trouver=true;}//s'il s'agit du bon agent alors on a trouvé et on sort de la boucle

        }



        if(!trouver) {agent=null;} //si sortie de la boucle car fin de la liste alors on élimine la dernière valeure de session



        return agent;

    }

    //Methode pour renvoyer les TypeSession accomplies pour un agent
    public List<TypeSession_M> getTypeSessionAchieved(String SID){
        Agent_M agent=this.getAgentFromSid(SID);
        return agent.getList_UV_achieved();
    }



    ////////////////////////// Ensemble des méthodes qui concernant la connexion  ////////////////////////////////

    // Méthode qui vérifie si un Agent est connecté (vérifie si le SID en paramètre est bien présent dans la hashmap)
    public boolean estConnexion(String SID){
        boolean rep=this.list_couple_SID_Agent_M.containsKey(SID);
        return rep;
    }


    // Ajoute un couple SID Agent_M à la map "list_couple_SID_Agent_M"
    public void ajouteSID_Agent_M(String sid, Agent_M unAgent_M){
        getList_couple_SID_Agent_M().put(sid,unAgent_M);
    }

    // Renvoie la hashmap complète des couples SID-Agent_M
    public Map<String, Agent_M> getList_couple_SID_Agent_M() {
        return list_couple_SID_Agent_M;
    }

    // TODO Cette méthode permet de renvoyer l'Agent_M de la map à partir de son SID, retourne null si pas dans la map
    // TODO cette méthode est-elle utile ?
    private Agent_M check_SID_Agent_M(String SID){
        return this.list_couple_SID_Agent_M.get(SID);
    }

}
