package ALFA_Group;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;



@Path("/authentification")
public class AuthentificationManager {
	@GET
	@Path("/login")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces("text/plain")
	public String authoriz(@QueryParam("pseudo") String login, @QueryParam("mdp") String mdp,
			@Context HttpServletRequest request) {
		GuichetMetier managerMetier = GuichetMetier.getGuichetMetier();
		for (Agent_M a : managerMetier.getGlobale_list_agents()) {
			if (a.getLogin().equals(login) && a.getPassword().equals(mdp)) {
				//Retourne la session en cours associée à la requete ou en crée une 
				HttpSession sessionInformatique = request.getSession();
				System.out.println(sessionInformatique);
				//Stocke nom d'agent dans la session sous le nom Name
				//sessionInformatique.setAttribute("name", a.getNomAgent());
				//System.out.println(a.getNomAgent());
				//genere un id session
				String sid = sessionInformatique.getId();
				managerMetier.getList_couple_SID_Agent_M().put(sid, a);
			
				return sid;
			}

		}
		return null;

	}
	 
    @POST
    @Path("/logout")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces("text/plain")
    public String logout(@Context HttpServletRequest request) {
    	/* Récupération et destruction de la session en cours */
		HttpSession sessionInformatique = request.getSession();
		sessionInformatique.invalidate();
		System.out.println(sessionInformatique);
		return "session detruite";
		
	}
}
