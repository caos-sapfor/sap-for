package ALFA_Group;

import java.util.ArrayList;

import java.util.List;


import javax.print.attribute.standard.Sides;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.spi.resource.Singleton;

@Singleton
@Path("{sid}/candidature")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class CandidatureManager {

    GuichetMetier guichetMetier;
    private List<Candidature_M> candidatureMList;
    private List<Candidature> candidatures= new ArrayList<Candidature>();
    // private List<CandidaturesDTO> candidaturesList;


    //contructeur
    public CandidatureManager() {
        candidatureMList = new ArrayList<Candidature_M>();
        guichetMetier= GuichetMetier.getGuichetMetier();
    }

    /**
     * Default action for the candidature manager: return all candidature
     * @return The list of candidature (all fields)
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public synchronized List<Candidature> getCandidatureList(@PathParam("sid") String sid) {
        //creation d'une liste DTO
        List<Candidature> candidatures;
        // vérification de la session
        if (this.guichetMetier.estConnexion(sid)) {
            // à reprendre car c'est liste de Candidature_M qui est récupérée
            candidatures = this.guichetMetier.getListCandidatures_M(sid);
            return candidatures;
        }
        return null;
    }

    /**
     * action Get candidature by idSession
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{idSessionUV}")
    public synchronized List<Candidature> getCandidatureByIdSession(@PathParam("sid") String sid, @PathParam("idSessionUV")String idSessionUV, @Context HttpServletRequest request) {
        List<Candidature> candidatures = new ArrayList<Candidature>();
        HttpSession sessionInformatique = request.getSession();
        if (sessionInformatique.getId().equals(sid)) {
            Agent_M agentMConnecte = (Agent_M) sessionInformatique.getAttribute("agentMConnecte");
            for (Candidature_M c : candidatureMList) {
                if (c.getSessionUV().getIdSessionUV().equals(idSessionUV))
                    candidatures.add(new Candidature(c)); //création et ajout de l'objet DTO à la liste des candidatures
            }
            return candidatures;
        }
        return null;
    }
    /**
     * action Get candidature by idAgent
     */
    /*@GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{idAgent}")
    public synchronized Candidature getCandidatureByIdSession(@PathParam("idAgent")String idAgent){
        Candidature candidature = null;
        for (Candidature c : candidatureMList){
            if (c.getIdAgent().equals(idAgent))
                candidature = c;
        }

        return candidature;  //aucune candidature correspondante à l'idAgent
    }*/
    /**
     * action Put candidature
     */
    @DELETE
    @Path("/idcandidature")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces("text/plain")
    public synchronized String deleteCandidature(@PathParam("sid") String sid, @QueryParam("idSession") String idSessionUV,@Context HttpServletRequest request ){
        //on récupère la session (la connexion)
        HttpSession sessionInformatique = request.getSession();
        if (sessionInformatique.getId().equals(sid)){
            Agent_M agentMConnecte = (Agent_M) sessionInformatique.getAttribute("agentMConnecte"); //recupère l'agent connecté
            for(Candidature_M candidatureM : agentMConnecte.getCandidatureMS()){ // parcours de la liste des candidatures de l'agent
                if (candidatureM.getSessionUV().getIdSessionUV().equals(idSessionUV)){// rechercher la cnadidature concerné
                    agentMConnecte.getCandidatureMS().remove(candidatureM);
                    return "Votre candidature a été Supprimée";
                }
                else
                    return "candidature non trouvée  ";
            }
        }
        return null;
    }

    @POST
    @Path("/qualifierCand")
    @Produces({MediaType.APPLICATION_JSON})
    public synchronized String updateCandidatureState(@PathParam("sid") String sid,@QueryParam("idSessionUV") String idSessionUV,@QueryParam("idCandidature") String idCandidature,@QueryParam("statutCandidature") String statutCandidature,@Context HttpServletRequest request){
        HttpSession sessionInformatique = request.getSession();
        if (sessionInformatique.getId().equals(sid)){ //verification de la connexion
            Agent_M agentMConnecte = (Agent_M) sessionInformatique.getAttribute("agentMConnecte"); //recupère l'agent connecté
            if (agentMConnecte.isGestionnaire()){
                for(Candidature_M candidatureM : candidatureMList){
                    if (candidatureM.getSessionUV().getIdSessionUV().equals(idSessionUV) && candidatureM.getIdCandidature().equals(idCandidature)){
                        candidatureM.setStatutCandidature(statutCandidature);
                    }

                }
            }
        }

        return "erreur";

    }


