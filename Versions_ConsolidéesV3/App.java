import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;

/**
 * User: llanoe
 * Date: 2018-03
 */

/**
 * @author Laurent Lanoe
 *         Client de test permettant de tester tous les userCases du projet SapFor
 */
public class App {
    public static void main(String[] args)  {




        // Get access to the service object
        ClientConfig config = new DefaultClientConfig();

        Client client = Client.create(config);
        WebResource service = client.resource(getBaseURI());



        // Test : Authentification Cas passant  : utilisation du login "DU0001" et mot de passe "mypassword003" -----------------
        // Objectif : vérifier la récupération du SID et l'ajout du couple SID-Agent à la Map du Guichet Metier
        System.out.println("----- TEST DE CONNEXION -----");
        System.out.println("Tentative de connexion avec le login : DU0001 et le mot de passe : mypassword003");

        String path = "authentification/login?pseudo=DU0001&mdp=mypassword003";
        URI uribis = getvariableURI(path);
        System.out.println("URL utilisé : "+getBaseURI()+"/"+path);

        String sid_recupere = service.uri(uribis)
                .accept("text/plain")
                .get(String.class);

        System.out.println("SID recupéré : "+sid_recupere);




        // Test : Afficher liste sessions accessibles pour l'agent --------------------------------------------------------
        System.out.println("----- TEST AFFICHER LISTE SESSIONS ACCESSIBLES POUR DU0001 "+"(SID : "+sid_recupere+")-----");
        path = sid_recupere+"/sessions";
        System.out.println("URL utilisé : "+getBaseURI()+"/"+path);
        System.out.println("Affichage des Sessions UV accessibles : ");

        List<SessionUV> sessionUVlist = service.path(sid_recupere+"/sessions")// on récupère la liste des SessionUV accesibles dans sessionUVlist
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<SessionUV>>(){});



        afficherlistSessionUV(sessionUVlist); // Affichage sur la console de la Liste de sessionsUV -> sessionUVlist










        // Test inscrire le candidat à une SessionUV ----------------------------------------------------------------
        System.out.println("Test d'inscription à une sessionUV");
        String idAgent = null;
        String idSession_cible = sessionUVlist.get(0).getIdSessionUV();
        String statut = null;
        String qualite_cand = "stagiaire";
        int position = 0;
        System.out.println("Test d'inscription à une sessionUV - id de la Session ciblé : "+idSession_cible);

        String idSessInfo = service.path(sid_recupere+"/sessions/idsess_info")
                .accept("text/plain")
                .get(String.class);
        System.out.println("idSesssion informatique est : "+idSessInfo);


       // path = sid_recupere+"/sessions/cand?idSessionUV="+idSession_cible+"&"+"qualite="+qualite_cand;
        //uribis = getvariableURI(path);
        Candidature candAsoumettre = new Candidature(idAgent, idSession_cible, statut, qualite_cand, position);

        Candidature maNouvCand = service.path(sid_recupere+"/sessions/candidature").type(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .post(new GenericType<Candidature>() {
        }, candAsoumettre);

        afficherUneCandidature(maNouvCand);




/*
        // Test : Afficher les détails d'une session ----------------------------------------------------------------
        System.out.println("----- TEST AFFICHER LES DETAILS d'UNE SESSION -----");
        SessionUV uneSession = sessionUVlist.get(0); // on sélectionne la première sessionUV de la liste -> sessionUVlist
        String idSession = uneSession.getIdSessionUV(); // on récupère ensuite l'idSession de cette sessionUV
        System.out.println("Affichage des détails de la session dont idSession est : "+idSession);

        SessionUV session = service.path(sid_recupere+"/sessions/"+idSession)
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<SessionUV>(){});


        afficherSessionUV(session);

        */




// Candidater SessionUV
// Suppr. Candidature
// Consulter Candidatures
// Afficher Sessions Accomplies
// Afficher les candidatures par sessionUV
// Accepter Candidature
// Refuser Candidature
// Mettre en attente
// Modifier dans la liste d'attente



        // Test Déconnexion

        Product nouveauProduit;
        List<Product> produits;



        // Get all products

        produits = service.path("produits")
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Product>>(){});

        afficher(produits);



        // Create a new product for testing the server
        nouveauProduit = new Product("Wrap aux feuilles d'acacia", 1.4f);

        // Add a new product using the POST HTTP method.
        //  managed by the Jersey framework
        service.path("produits").type(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .post(new GenericType<Product>() {
                }, nouveauProduit);

        Product premierProduit;

        premierProduit = service.path("produits/first")
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<Product>(){});

        service.path("produits/" + premierProduit.getUID())
                .delete();
        produits = service.path("produits")
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Product>>() {
                });
        afficher(produits);
    }




    // ----------------- Toutes les methodes d'affichage sur la console des différentes objets DTO ---------------------

    // affichage d'une liste de SessionsUV
    private static void afficherlistSessionUV(List<SessionUV> uneListeSessUV) {
        for(SessionUV p : uneListeSessUV){
            System.err.println("Id Session : "+p.getIdSessionUV());
            System.err.println("Intitule Session : "+p.getIntitule());
            System.err.println("Type Session : "+p.getTypeUV());
            System.err.println("Date Session : "+p.getDates());
        }
    }

    // affichage d'une SessionUV
    private static void afficherSessionUV(SessionUV uneSessionUV){
        System.err.println("Affichage des détails d'une session : ");
        System.err.println("Id Session : "+uneSessionUV.getIdSessionUV());
        System.err.println("Intitule Session : "+uneSessionUV.getIntitule());
        System.err.println("Type Session : "+uneSessionUV.getTypeUV());
        System.err.println("Date Session : "+uneSessionUV.getDates());
    }

    // affichage d'une Candidature
    private static void afficherUneCandidature(Candidature uneCanDTO){
        System.err.println("Affichage de la candidature : ");
        System.err.println("Id Agent : "+uneCanDTO.getIdAgent());
        System.err.println("Id Session : "+uneCanDTO.getIdSession());
        System.err.println("Statut : "+uneCanDTO.getStatut());
        System.err.println("Qualite : "+uneCanDTO.getQualite());
        System.err.println("Position : "+uneCanDTO.getPosition());
    }




    private static void afficher(List<Product> produits) {
        for(Product p : produits){
            System.err.println(p.getNom());
        }
    }

    private static URI getBaseURI() {
        URI uri =
                UriBuilder.fromUri("http://localhost:8080/DevManager00/webresources").build();
        return uri;
    }

    private static URI getvariableURI(String path) {
        URI uri =
                UriBuilder.fromUri(path).build();
        return uri;
    }


}