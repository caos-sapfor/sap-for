package ALFA_Group;

import com.sun.jersey.spi.resource.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Viviane Hounkanrin
 */
@Singleton
@Path("{sid}/sessions")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class SessionsManager {

    private GuichetMetier managerMetier = GuichetMetier.getGuichetMetier();


    // Retourne la liste des SessionsUV accessible pour l'agent en fonction du SID
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public synchronized List<SessionUV> getSessionList(@PathParam("sid") String sid, @Context HttpServletRequest request) {

        List<SessionUV> sessionUV_list = new ArrayList<SessionUV>();
        HttpSession sessionInformatique = request.getSession();
        //verification de la connexion
        if (sessionInformatique.getId().substring(0, 4).equals(sid.substring(0, 4))) {

            List<SessionUV_M> authorized_sessions = managerMetier.getSessionsAccessibles(sid);
            for (int i = 0; i < authorized_sessions.size(); i++){
                String idSession = authorized_sessions.get(i).getIdSessionUV();
                String intitule = authorized_sessions.get(i).getIntituleSessionUV();
                String typeUV = authorized_sessions.get(i).getTypeUV().getIntitule();
                List<String> date = authorized_sessions.get(i).getDates();

                sessionUV_list.add(new SessionUV(idSession, intitule, typeUV, date));
            }
            return sessionUV_list;
        }
        return null;
    }

    //Renvoie les détails d'une SessionUV en fonction de l'idSession envoyé
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{idSessionUV}")
    public synchronized SessionUV getSessionsDetails(@PathParam("sid") String sid, @PathParam("idSessionUV") String idSessionUV, @Context HttpServletRequest request) {
        //recuperation de la session
        HttpSession sessionInformatique2 = request.getSession();
        // verification si l'agent vraiment connecté


        if (sessionInformatique2.getId().substring(0, 4).equals(sid.substring(0, 4))) {
            //GuichetMetier managerMetier = GuichetMetier.getGuichetMetier();
            SessionUV sessionD = new SessionUV(managerMetier.getDetailsSession(idSessionUV));

            return sessionD;
            }
        return null;
    }

    // Pour candidater à une session -> controles préalables de la possibilité ou non de candidater en tant que gestionnaire
    @POST
    @Path("/candidature")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public synchronized Candidature addCandidature(@PathParam("sid") String sid, Candidature uneCand, @Context HttpServletRequest request) {
        //recuperation de la session
        HttpSession sessionInformatique = request.getSession();
        // verification si l'agent vraiment connecté
        if (sessionInformatique.getId().substring(0, 4).equals(sid.substring(0, 4))) {
            //GuichetMetier managerMetier = GuichetMetier.getGuichetMetier();
            // Vérification que l'agent a bien le droit de candidater à la session ciblée
            List<SessionUV_M> sessionsAccessibles = managerMetier.getSessionsAccessibles(sid);
            List<String> id_des_sessions_accessibles = new ArrayList<>(); // on créé une liste qui va contenir les idSession des Sessions accessibles pour l'agent
            for (int i = 0; i < sessionsAccessibles.size(); i++) {
                String idSession = sessionsAccessibles.get(i).getIdSessionUV();
                id_des_sessions_accessibles.add(idSession);
            }

            if (id_des_sessions_accessibles.contains(uneCand.getIdSession())){
                // Si qualité est "stagiaire" alors on créé la candidature, si c'est formateur, on vérifie qu'il peut être formateur
                // sur la session avant de créer la candidature
                if (uneCand.getQualite().equals("stagiaire")){
                    Candidature newCand = managerMetier.candConverter(managerMetier.createCandidature(sid,uneCand.getIdSession(), uneCand.getQualite()));
                    return newCand;

                } else if (uneCand.getQualite().equals("formateur")){
                    Agent_M candidat = managerMetier.getAgentFromSid(sid);
                    SessionUV_M sessionCible = managerMetier.getDetailsSession(uneCand.getIdSession());
                    if (managerMetier.isAuthorizedAsFormer(candidat, sessionCible)){  // test de vérif si le candidat est authisé en tant que formateur sur la session
                        Candidature_M nouvCand = managerMetier.createCandidature(sid,uneCand.getIdSession(), uneCand.getQualite());
                        Candidature newCand = managerMetier.candConverter(nouvCand);
                        return newCand;
                    }
                    return null;
                }
                return null;
            }
            return null;
        }
        return null;
    }


    @GET
    @Produces("text/plain")
    @Path("/idsess_info")
    public synchronized String getSessions_id_Informatique(@Context HttpServletRequest request) {
        //recuperation de la session
        HttpSession sessionInformatique = request.getSession();
        return sessionInformatique.getId();
    }
/*
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public synchronized String creerTypeUV(TypeUV unTypeUV){
        GuichetMetier managerMetier = GuichetMetier.getGuichetMetier();
        TypeSession_M nouvtype = managerMetier.createTypeSession(unTypeUV);
        String titre = nouvtype.getIntitule();
        String retour = titre+" a bien été créé";
        return retour;
    }*/


	/*@POST
	@Path("/cand")
	@Produces({MediaType.APPLICATION_JSON})
	//@Consumes({MediaType.APPLICATION_JSON})

	public synchronized String CandidaterSessionsUV (@QueryParam("sid") String sid, @QueryParam("idSessionUV") String idSessionUV, @QueryParam("qualite") String qualite, @QueryParam("date") Date date, @QueryParam("prerequis")  String prerequis, @Context HttpServletRequest request){
		List<SessionUV> sessionUVS = new ArrayList<SessionUV>();
		Candidature_M candidatureM;
		SessionUV_M session;
			//recuperation de la session
		HttpSession sessionInformatique = request.getSession();
		// verification si l'agent vraiment connecté
		if (sessionInformatique.getId().equals(sid)) {
			Agent_M agentMConnecte = (Agent_M) sessionInformatique.getAttribute("agentMConnecte");
			//vérification de la qualité



			//verification de la validité de la date
			for ( session : this.sessionList)


				if (session.getDate().equals(date) && session.getIdSessionUV().equals(idSessionUV)


		return "";
	}
}
}

	  // Add a new product to the list.
	/*@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public synchronized SessionUV_M addSession(SessionUV_M s) {
		this.sessionList.add(s);
		return s;
	}

	// get session by id
	//  @param id is the id of the session
	// @return the session with the corresponding id, null if there's none.
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("find")
	public synchronized SessionUV_M getSessionByName(@QueryParam("id") String id) {
		for (SessionUV_M s : this.productList) {
			if (s.getID().equals(id))
				return s;
		}
		return null;
	}

	// Get a session with its title(intitulé)
	// @param intitule is the title of the session
	// @return the session with the corresponding title, null if there's none.
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("find")
	public synchronized SessionUV_M getSessionByIntitule(@QueryParam("intitule") String intitule) {
		for (SessionUV_M s : this.sessionList) {
			if (s.getIntitule().equals(intitule))
				return s;
		}
		return null;
	}*/

}
