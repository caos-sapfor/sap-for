package ALFA_Group;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Path("/authentification")
public class AuthentificationManager {

    private GuichetMetier managerMetier;

    public AuthentificationManager(){
        managerMetier = GuichetMetier.getGuichetMetier();
    }

	@GET
	@Path("/login")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces("text/plain")
	public String authoriz (@QueryParam("pseudo") String login, @QueryParam("mdp") String mdp,	@Context HttpServletRequest request) {
        if (managerMetier.agentexist(login, mdp) != null) {
            Agent_M a = managerMetier.agentexist(login, mdp);
            HttpSession sessionInformatique = request.getSession();
            String sid = sessionInformatique.getId();
            managerMetier.getList_couple_SID_Agent_M().put(sid, a);
            return sid;

        } else {

            String A1 = "Le couple ";
            String A2 = login;
            String A3 = " / ";
            String A4 = mdp;
            String A5 = "n'existe pas dans le systeme";
            String res = A1+A2+A3+A4+A5;

            return res;
        }
    }

    /*
		String res = null;
        for (Agent_M a : managerMetier.getGlobale_list_agents()) {
			if (a.getLogin().equals(login) && a.getPassword().equals(mdp)) {
				//Retourne la session en cours associée à la requete ou en crée une 
				HttpSession sessionInformatique = request.getSession();
				System.out.println(sessionInformatique);
				//Stocke nom d'agent dans la session sous le nom Name
				//sessionInformatique.setAttribute("name", a.getNomAgent());
				//System.out.println(a.getNomAgent());
				//genere un id session
				String sid = sessionInformatique.getId();
				managerMetier.getList_couple_SID_Agent_M().put(sid, a);
				res = sid;
			
				break;
			}

		}
		return res;

	}
	 */
    @POST
    @Path("/logout")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces("text/plain")
    public String logout(@Context HttpServletRequest request) {
    	/* Récupération et destruction de la session en cours */
		HttpSession sessionInformatique = request.getSession();
		sessionInformatique.invalidate();
		System.out.println(sessionInformatique);
		return "session detruite";
		
	}
}
